package main

import (
	"fmt"
	"os"
	"strings"

	"git.gitorious.org/trantor/trantor.git/database"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	DB_IP      = "127.0.0.1"
	DB_NAME    = "trantor"
	books_coll = "books"
)

func main() {
	session, err := mgo.Dial(DB_IP)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer session.Close()
	db := database.Init(DB_IP, DB_NAME)
	defer db.Close()

	coll := session.DB(DB_NAME).C(books_coll)
	books := coll.Find(bson.M{"$nor": []bson.M{{"active": true}}}).Iter()
	var b database.Book
	for books.Next(&b) {
		query := buildQuery(b)
		if query == "" {
			continue
		}

		_, num, err := db.GetBooks(query, 0, 0)
		if err == nil && num == 0 {
			fmt.Println(b.Title)
			db.ActiveBook(b.Id)
		}
	}
}

func buildQuery(book database.Book) string {
	query := "title:" + strings.Replace(book.Title, ":", " ", -1)
	if len(book.Lang) > 0 && len(book.Lang[0]) >= 2 {
		query += " lang:" + strings.ToLower(book.Lang[0][:2])
	}
	return query
}
