package main

import (
	"os"

	log "github.com/cihub/seelog"
	"github.com/meskio/epubgo"
	mongo "gitlab.com/trantor/tools/mongo2psql/mongo"
	"gitlab.com/trantor/trantor/lib/database"
	"gitlab.com/trantor/trantor/lib/parser"
	"gitlab.com/trantor/trantor/lib/storage"
)

const (
	mongoIP    = "127.0.0.1"
	mongoName  = "trantor"
	dbAddr     = "localhost:5432"
	dbUser     = "trantor"
	dbPassword = "trantor"
	dbName     = "trantor"
	storePath  = "store"
	epubFile   = "book.epub"
)

func main() {
	mdb := mongo.Init(mongoIP, mongoName)
	defer mdb.Close()

	pdb, err := database.Init(database.Options{
		Addr:     dbAddr,
		User:     dbUser,
		Password: dbPassword,
		Name:     dbName,
	})
	if err != nil {
		log.Critical("Problem initializing database: ", err)
		os.Exit(1)
	}
	defer pdb.Close()

	//store, err := storage.Init(storePath, true)
	if err != nil {
		log.Critical("Problem initializing store: ", err)
		os.Exit(1)
	}

	insertUsers(mdb, pdb)
	insertNews(mdb, pdb)
	//insertBooks(mdb, pdb, store)
}

func insertUsers(mdb *mongo.DB, pdb *database.DB) {
	users, err := mdb.GetUsers()
	if err != nil {
		log.Error("Error getting users: ", err)
		return
	}

	for _, user := range users {
		err := pdb.AddRawUser(user.User, user.Pass, user.Salt, user.Role)
		if err != nil {
			log.Error("Error writting user (", user.User, ", ", user.Pass, ", ", user.Salt, ", ", user.Role, "): ", err)
		}
	}
}

func insertNews(mdb *mongo.DB, pdb *database.DB) {
	news, err := mdb.GetNews(100000, 100000)
	if err != nil {
		log.Error("Error getting news: ", err)
		return
	}

	for i := len(news) - 1; i >= 0; i-- {
		new := news[i]
		err := pdb.AddRawNews(new.Text, new.Date)
		if err != nil {
			log.Error("Error writting news (", new.Text, ", ", new.Date, "): ", err)
		}
	}
}

func insertBooks(mdb *mongo.DB, pdb *database.DB, store *storage.Store) {
	booksIter := mdb.GetBooksIter()
	var b mongo.Book
	for booksIter.Next(&b) {
		processBook(b, pdb, store)
	}
	if err := booksIter.Close(); err != nil {
		log.Error("Some error happend with the iterator: ", err)
	}
}

func processBook(mbook mongo.Book, pdb *database.DB, store *storage.Store) {
	log.Info("Book written: ", mbook.Id, " - ", mbook.Title, " - ", mbook.Author)

	file, err := store.Get(mbook.Id, epubFile)
	if err != nil {
		log.Error("Error opening ", mbook.Id, " from storage: ", err)
		return
	}
	defer file.Close()

	epub, err := epubgo.Load(file, int64(mbook.FileSize))
	if err != nil {
		log.Error("Error loading epub ", mbook.Id, ": ", err)
		return
	}
	defer epub.Close()

	pbook := database.Book{
		ID:          mbook.Id,
		Title:       mbook.Title,
		Authors:     mbook.Author,
		Contributor: mbook.Contributor,
		Publisher:   mbook.Publisher,
		Description: mbook.Description,
		Tags:        parser.ParseSubject(mbook.Subject),
		Date:        mbook.Date,
		Lang:        parser.GuessLang(epub, mbook.Lang),
		Isbn:        parser.ISBN(mbook.Isbn),
		FileSize:    mbook.FileSize,
		Cover:       mbook.Cover,
		Active:      mbook.Active,
		UploadDate:  mbook.MongoId.Time(),
	}
	err = pdb.AddBook(pbook)
	if err != nil {
		log.Error("Error writting to db: ", mbook.Id, " - ", mbook.Title, ": ", err)
	}
}
