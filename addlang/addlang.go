package main

import (
	log "github.com/cihub/seelog"

	"fmt"
	"os"
	"strings"

	"git.gitorious.org/trantor/trantor.git/database"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	DB_IP      = "127.0.0.1"
	DB_NAME    = "trantor"
	books_coll = "books"
)

func main() {
	session, err := mgo.Dial(DB_IP)
	if err != nil {
		log.Critical(err)
		os.Exit(1)
	}
	defer session.Close()

	coll := session.DB(DB_NAME).C(books_coll)
	books := coll.Find(bson.M{}).Iter()
	var b database.Book
	for books.Next(&b) {
		fmt.Println(b.Title)
		lang := ""
		if len(b.Lang) > 0 && len(b.Lang[0]) >= 2 {
			lang = strings.ToLower(b.Lang[0][0:2])
		}
		book := map[string]interface{}{
			"_lang": lang,
		}
		err := coll.Update(bson.M{"id": b.Id}, bson.M{"$set": book})
		if err != nil {
			fmt.Println(err)
		}
	}
}
