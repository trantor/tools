package main

import log "github.com/cihub/seelog"

import (
	"gitlab.com/trantor/trantor/database"
	"gitlab.com/trantor/trantor/parser"
)

const (
	DB_IP   = "127.0.0.1"
	DB_NAME = "trantor"
)

func main() {
	db := database.Init(DB_IP, DB_NAME)
	defer db.Close()

	booksIter := db.GetBooksIter()
	var b database.Book
	for booksIter.Next(&b) {
		processIsbn(b, db)
	}
	if err := booksIter.Close(); err != nil {
		log.Error("Some error happend with the iterator: ", err)
	}
}

func processIsbn(book database.Book, db *database.DB) {
	if book.Isbn == "" {
		return
	}

	isbn := parser.ISBN(book.Isbn)
	if isbn == "" {
		log.Error(book.Title, " (", book.Id, ") invalid isbn: ", book.Isbn)
		return
	}

	log.Info(book.Title, " (", book.Id, "): ", book.Isbn, " -> ", isbn)
	update := map[string]interface{}{"isbn": isbn}
	err := db.UpdateBook(book.Id, update)
	if err != nil {
		log.Error("Update problem: ", err)
	}
}
