package main

import log "github.com/cihub/seelog"

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"git.gitorious.org/go-pkg/epubgo.git"
	"git.gitorious.org/trantor/trantor.git/database"
	"git.gitorious.org/trantor/trantor.git/storage"
	"github.com/rainycape/cld2"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	DB_IP      = "127.0.0.1"
	DB_NAME    = "trantor"
	books_coll = "books"
	EPUB_FILE  = "book.epub"
	STORE_PATH = "store/"
)

func main() {
	session, err := mgo.Dial(DB_IP)
	if err != nil {
		log.Critical(err)
		os.Exit(1)
	}
	defer session.Close()
	db := database.Init(DB_IP, DB_NAME)
	defer db.Close()

	store, err := storage.Init(STORE_PATH)
	if err != nil {
		log.Critical(err)
		os.Exit(1)
	}

	coll := session.DB(DB_NAME).C(books_coll)
	books := coll.Find(bson.M{}).Iter()
	var b database.Book
	for books.Next(&b) {
		processLang(b, store, db)
	}
}

func processLang(book database.Book, store *storage.Store, db *database.DB) {
	file, err := store.Get(book.Id, EPUB_FILE)
	if err != nil {
		log.Error("Error opening ", book.Id, " from storage: ", err)
		return
	}
	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		log.Error("Error getting info ", book.Id, ": ", err)
		return
	}

	epub, err := epubgo.Load(file, info.Size())
	if err != nil {
		log.Error("Error loading epub ", book.Id, ": ", err)
		return
	}
	defer epub.Close()

	spine, err := epub.Spine()
	if err != nil {
		log.Error("Error getting spine ", book.Id, ": ", err)
		return
	}

	var err1 error
	err1 = nil
	lang := []string{}
	for err1 == nil {

		html, err := spine.Open()
		err1 = spine.Next()
		if err != nil {
			log.Error("Error getting spine file ", book.Id, ": ", err)
			continue
		}
		defer html.Close()

		buff, err := ioutil.ReadAll(html)
		if err != nil {
			log.Error("Error getting buffer ", book.Id, ": ", err)
			continue
		}
		lang = append(lang, cld2.Detect(string(buff)))
	}

	updateLang(lang, book, db)
}

func updateLang(langs []string, book database.Book, db *database.DB) {
	count := map[string]int{}
	for _, l := range langs {
		count[l]++
	}

	lang := "un"
	maxcount := 0
	for l, c := range count {
		if c > maxcount && l != "un" {
			lang = l
			maxcount = c
		}
	}
	if lang == "un" {
		return
	}

	orig_lang := "un"
	if len(book.Lang) > 0 && len(book.Lang[0]) >= 2 {
		orig_lang = strings.ToLower(book.Lang[0][0:2])
	}

	if orig_lang != lang {
		data := map[string]interface{}{"lang": []string{lang}}
		db.UpdateBook(book.Id, data)
		fmt.Println(book.Title, "(", book.Id, "): ", orig_lang, "->", lang)
	}
}
